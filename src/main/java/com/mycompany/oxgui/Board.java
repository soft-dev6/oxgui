/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgui;

/**
 *
 * @author hanam
 */
public class Board {
    private char[][] table = {{'-', '-', '-',}, {'-', '-', '-',}, {'-', '-', '-',}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    public boolean win =false;
    public boolean draw =false;
    
    public Board(Player o, Player x){
        this.o=o;
        this.x=x;
        this.currentPlayer=o;
        this.count=0;
    }

    public char[][] getTable() {
        return table;
    }

    public void setTable(char[][] table) {
        this.table = table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public int getCount() {
        return count;
    }
    
    private void switchPlayer(){
        if(currentPlayer!=x) currentPlayer = x;
        else
            currentPlayer=o;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }
    
    
    public boolean setRowCol(int row, int col){
        if(isWin()||isDraw()) return false;
        if(row>3||col>3||row<1||col<1) return false;
        if(table[row-1][col-1]!='-') return false;
        table[row-1][col-1]= currentPlayer.getSymbol();
        if(checkWin(row,  col)){
            updateStat();
            this.win = true;
            return true;
        }
        if(checkDraw()){
             x.draw();
             o.draw();
             this.draw = true;
             return true;
        } 
        count++;
        switchPlayer();
        return true;
    }

    public void updateStat() {
        if( this.currentPlayer==o){
            o.win();
            x.loss();
        }else{
            x.win();
            o.loss();
        }
    }
    
    public  boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

     public  boolean checkWin(int row, int col){
        if(checkVertical(col)){
            return true;
        }else if(checkHorizontal(row)){
            return true;
        }else if(checkDigonal1()){
             return true;
        }else if(checkDigonal2()){
            return true;
        }
        return false;
    }
     
     public  boolean checkVertical(int col) {
        for(int r =0;r<table.length;r++){ //r = row
            if(table[r][col-1] != currentPlayer.getSymbol()) return false;
        }
        return true;
    }

    public  boolean checkHorizontal(int row) { // c = col
        for(int c = 0; c< table.length;c++){
            if(table[row-1][c]!=currentPlayer.getSymbol()) return false;
        }
        return true;
    }

    public  boolean checkDigonal1() { //11 22 33
        for(int i = 0; i< table.length;i++){
            if(table[i][i]!=currentPlayer.getSymbol()) return false;
        }
        return true;
    }
    
    public boolean checkDigonal2() { //13 22 31
        for(int i = 0; i<table.length;i++){
            if(table[i][2-i]!=currentPlayer.getSymbol()) return false;
        }
        return true;
    }
}
